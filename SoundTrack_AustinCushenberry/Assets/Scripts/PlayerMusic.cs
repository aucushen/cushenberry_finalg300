﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;// Added sound control

public class PlayerMusic : MonoBehaviour
{
    public AudioClip skatesound;
    public AudioClip skatesound2;
    public AudioClip skatesound3;


    public AudioMixer mixer;
    public AudioMixerSnapshot[] snapshots;
    public float [] weights;
    public float tranlen;
    private AudioSource source;
    private float volLowRange=.5f;//vol change
    private float volHighRange=1.0f;//vol change

    private void Awake()
    {
        source = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)) // ollie sound on spacebar press
        {
            float vol = Random.Range(volLowRange, volHighRange);
            source.PlayOneShot(skatesound, vol);

        }

        if (Input.GetKeyDown (KeyCode.LeftAlt))

        {
            float vol = Random.Range(volLowRange, volHighRange); //skid sound on press
            source.PlayOneShot(skatesound2, vol);
        }

        if (Input.GetKeyDown(KeyCode.RightAlt))

        {
            float vol = Random.Range(volLowRange, volHighRange);//say hey on press
            source.PlayOneShot(skatesound3, vol);
        }


    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("MusicTrig"))
        {
            weights[0] = 0.0f;
            weights[1] = 1.0f;
            mixer.TransitionToSnapshots(snapshots, weights, tranlen);

        }
        
       
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("MusicTrig"))
        {
            weights[0] = 1.0f;
            weights[1] = 0.0f;
            mixer.TransitionToSnapshots(snapshots, weights, tranlen);

        }
    }


}

