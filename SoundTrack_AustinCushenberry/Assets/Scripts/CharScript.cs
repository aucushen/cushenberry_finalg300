﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharScript : MonoBehaviour

// Start is called before the first frame update


{
    public int m_PlayerNumber = 1;              // Used to identify which tank belongs to which player.  This is set by this tank's manager.
    public float m_Speed = 12f;                 // How fast the tank moves forward and back.
    public float m_TurnSpeed = 180f;            // How fast the tank turns in degrees per second.
    public float m_timeleft = 260.0f;           // The amount of Time to explore enviroment
    public Transform teleportpoint1;            // locations to teleport to
    public Transform teleportpoint2;            // locations to teleport to
    public Transform teleportpoint3;            // locations to teleport to
    public Transform teleportpoint4;            // locations to teleport to
    bool onGround = true;
    public float jumpPower;
    //unused VARs
    //public AudioSource m_MovementAudio;       // Reference to the audio source used to play engine sounds. NB: different to the shooting audio source.
    //public AudioClip m_EngineIdling;          // Audio to play when the tank isn't moving.
    //public AudioClip m_EngineDriving;         // Audio to play when the tank is moving.
    //public float m_PitchRange = 0.2f;         // The amount by which the pitch of the engine noises can vary.



    private string m_TurnAxisName;              // The name of the input axis for turning.
    private Rigidbody m_Rigidbody;              // Reference used to move the tank.
    private float m_MovementInputValue;         // The current value of the movement input.
    private float m_TurnInputValue;             // The current value of the turn input.
                                                //private float m_OriginalPitch;              // The pitch of the audio source at the start of the scene.
    private Animator animator;




    private void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
    }








    private void Start()
    {

    }


    private void Update()
    {
        // Store the value of both input axes.
        m_MovementInputValue = Input.GetAxis("Vertical");
        m_TurnInputValue = Input.GetAxis("Horizontal");


        m_timeleft -= Time.deltaTime;
        if (m_timeleft < 0)
        {
            GameOver();
        }
        //EngineAudio();
        onGround = Physics.Raycast(transform.position, Vector3.down, 1.16f);

        if (Input.GetKeyDown(KeyCode.Space) && onGround)// jump and on the ground
        {
            animator.SetBool("ollie", true);
        }
        if (Input.GetKeyUp(KeyCode.Space) && onGround)
        {
            Jump();
            animator.SetBool("ollie", false);
        }







            if (Input.GetKeyDown(KeyCode.X)    && onGround)
        {
            m_Speed = m_Speed * 1.75f; //speed up while button down
        }
        if (Input.GetKeyUp(KeyCode.X) && onGround)
        {
            m_Speed = m_Speed / 1.75f; //Reduse speed after button let go
        }
    }

    private void FixedUpdate()
    {
        // Adjust the rigidbodies position and orientation in FixedUpdate.
        Move();
        Turn();
        bool crouch = Input.GetKey(KeyCode.C);
    }


    private void Move()
    {
        // Create a vector in the direction the tank is facing with a magnitude based on the input, speed and the time between frames.
        Vector3 movement = transform.forward * m_MovementInputValue * m_Speed * Time.deltaTime;

        // Apply this movement to the rigidbody's position.
        m_Rigidbody.velocity += movement;//.MovePosition(m_Rigidbody.position + movement);
    }

    void GameOver()

    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    void Jump()
    {
        m_Rigidbody.AddForce(Vector3.up * jumpPower);
    }

    void OnTriggerEnter(Collider other)
    {


        if (other.CompareTag("Player"))
        {
            other.transform.position = teleportpoint1.position;

        }

    }

    void Turn()
    {
        // Determine the number of degrees to be turned based on the input, speed and time between frames.
        float turn = m_TurnInputValue * m_TurnSpeed * Time.deltaTime;

        // Make this into a rotation in the y axis.
        Quaternion turnRotation = Quaternion.Euler(0f, turn, 0f);

        // Apply this rotation to the rigidbody's rotation.
        //m_Rigidbody.MoveRotation(m_Rigidbody.rotation * turnRotation);
        m_Rigidbody.angularVelocity += new Vector3(0f,turn,0f);
    }


}  